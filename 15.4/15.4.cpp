﻿#include <iostream>

void function(int N, int even)
{

    int i = 0;

    if (even == 0)
    {
        std::cout << std::endl;
        for (int i = 1; i <= N; i++)
        {
            if (i % 2 == 0)
                std::cout << i << std::endl;
        }
    }
    else
    {
        std::cout << std::endl << "0\n";
        for (int i = 0; i <= N; i++)
        {
            if (i % 2 != 0)
                std::cout << i << std::endl;
        }
    }
}


int main()
{
    const int N = 10;
    int even;

    /*
    0 - четные числа
    1 - нечетные числа
    */
    std::cout << "Enter even: 0 or 1\n";
    std::cin >> even;

    function(N, even);

}